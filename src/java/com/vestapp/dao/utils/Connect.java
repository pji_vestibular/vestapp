package com.vestapp.dao.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author Alexandre
 */
public class Connect{
    
    private static Connection conn;
    private static final Connect instance = new Connect();
    
    private final String DB = "vestapp";
    private final String CLASS = "com.mysql.jdbc.Driver";
    private final String USER = "root";
    private final String PW = "root";
    private final String URL = "jdbc:mysql://localhost:3306/"+DB;

    
    private Connect(){   
        open();
    }
   
    public static Connect getInstance() {
            return instance;
    }
    
    public synchronized static Connection getConnection(){
        return conn;
    }

    private void open() {
        try {
            Class.forName(CLASS);
            conn = DriverManager.getConnection(URL, USER, PW);
            conn.setAutoCommit(false);
        } catch (ClassNotFoundException  e) {
            throw new RuntimeException("Classe não encontrada.  - " + e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException("Erro SQL",e);
        }
    }
    
    public synchronized void executeUpdate(PreparedStatement ps){
        try{
            ps.executeUpdate();
            confirm();
        }catch(SQLException e){
            abort();
            throw new RuntimeException("Erro de ao realizar atualizacao - " + e.toString());
        }
        
    }
    public synchronized ResultSet executeQuery(PreparedStatement ps){
        ResultSet rs = null;
        try{
            rs = ps.executeQuery();
        }catch(SQLException e){
            throw new RuntimeException("Erro de ao realizar busca - " + e.toString());
        }
        return rs;
    }
    

    private void confirm() {
        try{
            conn.commit();            
        }catch(SQLException e){
            throw new RuntimeException("Erro de commit - " + e.toString());
        }
    }

    private void abort() {
        try{
            conn.rollback();
        }catch(SQLException e){
            throw new RuntimeException("Erro de rollback - " + e.toString());
        }
    }
       
}
