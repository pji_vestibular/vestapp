/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Alexandre
 * Created: 09/10/2016
 */

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema vestapp
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema vestapp
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `vestapp` DEFAULT CHARACTER SET utf8 ;
USE `vestapp` ;

-- -----------------------------------------------------
-- Table `vestapp`.`instituicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vestapp`.`instituicao` (
  `idInstituicao` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `codigo` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`idInstituicao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vestapp`.`concurso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vestapp`.`concurso` (
  `idConcurso` INT(11) NOT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `valor` DOUBLE NULL DEFAULT NULL,
  `Instituicao_idInstituicao` INT(11) NOT NULL,
  PRIMARY KEY (`idConcurso`),
  INDEX `fk_Concurso_Instituicao1_idx` (`Instituicao_idInstituicao` ASC),
  CONSTRAINT `fk_Concurso_Instituicao1`
    FOREIGN KEY (`Instituicao_idInstituicao`)
    REFERENCES `vestapp`.`instituicao` (`idInstituicao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vestapp`.`estudante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vestapp`.`estudante` (
  `idEstudante` INT(11) NOT NULL AUTO_INCREMENT,
  `nome_colegio` VARCHAR(45) NULL DEFAULT NULL,
  `numero_matricola` VARCHAR(45) NULL DEFAULT NULL,
  `login` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `data_nascimento` DATETIME NULL DEFAULT NULL,
  `senha` VARCHAR(45) NULL,
  PRIMARY KEY (`idEstudante`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vestapp`.`materia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vestapp`.`materia` (
  `idMateria` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `conteudo` VARCHAR(45) NULL DEFAULT NULL,
  `Concurso_idConcurso` INT(11) NOT NULL,
  PRIMARY KEY (`idMateria`),
  INDEX `fk_Materia_Concurso1_idx` (`Concurso_idConcurso` ASC),
  CONSTRAINT `fk_Materia_Concurso1`
    FOREIGN KEY (`Concurso_idConcurso`)
    REFERENCES `vestapp`.`concurso` (`idConcurso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vestapp`.`professor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vestapp`.`professor` (
  `idProfessor` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(45) NULL DEFAULT NULL,
  `email_universidade` VARCHAR(45) NULL DEFAULT NULL,
  `login` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `data_nascimento` DATETIME NULL DEFAULT NULL,
  `senha` VARCHAR(45) NULL,
  PRIMARY KEY (`idProfessor`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vestapp`.`resumo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vestapp`.`resumo` (
  `idResumo` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `Materia_idMateria` INT(11) NOT NULL,
  `Professor_idProfessor` INT(11) NOT NULL,
  PRIMARY KEY (`idResumo`),
  INDEX `fk_Resumo_Materia1_idx` (`Materia_idMateria` ASC),
  INDEX `fk_Resumo_Professor1_idx` (`Professor_idProfessor` ASC),
  CONSTRAINT `fk_Resumo_Materia1`
    FOREIGN KEY (`Materia_idMateria`)
    REFERENCES `vestapp`.`materia` (`idMateria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Resumo_Professor1`
    FOREIGN KEY (`Professor_idProfessor`)
    REFERENCES `vestapp`.`professor` (`idProfessor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `vestapp`.`rotina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vestapp`.`rotina` (
  `idRotinacol` INT(11) NOT NULL AUTO_INCREMENT,
  `data_inicial` DATETIME NULL DEFAULT NULL,
  `data_final` DATETIME NULL DEFAULT NULL,
  `descricao` VARCHAR(255) NULL DEFAULT NULL,
  `Concurso_idConcurso` INT(11) NOT NULL,
  `Estudante_idEstudante` INT(11) NOT NULL,
  PRIMARY KEY (`idRotinacol`, `Concurso_idConcurso`, `Estudante_idEstudante`),
  INDEX `fk_Rotina_Concurso1_idx` (`Concurso_idConcurso` ASC),
  INDEX `fk_Rotina_Estudante1_idx` (`Estudante_idEstudante` ASC),
  CONSTRAINT `fk_Rotina_Concurso1`
    FOREIGN KEY (`Concurso_idConcurso`)
    REFERENCES `vestapp`.`concurso` (`idConcurso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Rotina_Estudante1`
    FOREIGN KEY (`Estudante_idEstudante`)
    REFERENCES `vestapp`.`estudante` (`idEstudante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
