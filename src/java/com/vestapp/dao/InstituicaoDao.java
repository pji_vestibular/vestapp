package com.vestapp.dao;

import com.vestapp.dao.utils.Connect;
import com.vestapp.model.Instituicao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InstituicaoDao extends PatternDao<Instituicao>{
    
    private StringBuilder insertSQL = new StringBuilder()
            .append("INSERT INTO INSTITUICAO ")
            .append("(nome, codigo) ")
            .append("VALUES ")
            .append("(?,?)");
    
    private StringBuilder updateSQL = new StringBuilder()
            .append("UPDATE INSTITUICAO ")
            .append("SET nome=?, codigo=? ")
            .append("WHERE idInstituicao=?");
    
    private StringBuilder deleteSQL = new StringBuilder()
            .append("DELETE FROM INSTITUICAO ")
            .append("WHERE idInstituicao=?");
    
    private StringBuilder selectIdSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM INSTITUICAO ")
            .append("WHERE idInstituicao = ? ");
	
    private StringBuilder selectAllSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM INSTITUICAO ");
    
    private static InstituicaoDao instance = new InstituicaoDao();
    
    private InstituicaoDao(){
    }
    
    public static InstituicaoDao getInstance(){
        return instance;
    }
    
    @Override
    public void insert(Instituicao object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(insertSQL.toString());
            ps.setString(cont++, object.getNome());
            ps.setString(cont++, object.getCodigo());
           
            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void update(Instituicao object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(updateSQL.toString());
            ps.setString(cont++, object.getNome());
            ps.setString(cont++, object.getCodigo());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void delete(Instituicao object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(deleteSQL.toString());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public Instituicao findById(int id) {
        int cont = 1;
        Connect conexao = Connect.getInstance();
        Instituicao instituicao = null;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectIdSQL.toString());
            ps.setInt(cont++, id);
            rs = conexao.executeQuery(ps);
            if(rs.next()){
                    instituicao = populateObject(rs);
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return instituicao;
    }
    
    private String prepareStringSQLForFilter(Instituicao filtro){
        StringBuilder sb = new StringBuilder(selectAllSQL.toString());
        sb.append(" WHERE ");

        boolean and = false;

        if(filtro.getNome() != null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" nome = ? ");
        }
        if(filtro.getCodigo()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" codigo = ? ");
        }
        
        return sb.toString();
    }
    
    private PreparedStatement prepareStatementForFilter(Instituicao filtro, PreparedStatement ps) throws SQLException{
        int cont = 1;
        if(filtro.getNome() != null)
            ps.setString(cont++, filtro.getNome());
        if(filtro.getCodigo()!= null)
            ps.setString(cont++, filtro.getCodigo());
        
        return ps;
    }

    public List<Instituicao> findByFilter(Instituicao filter) {
        Connect conexao = Connect.getInstance();
        List<Instituicao> instituicoes = new ArrayList<Instituicao>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(prepareStringSQLForFilter(filter));
            rs = conexao.executeQuery(prepareStatementForFilter(filter, ps));
            while(rs.next()){
                    instituicoes.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return instituicoes;
    }

    @Override
    public List<Instituicao> findAll() {
        Connect conexao = Connect.getInstance();
        List<Instituicao> instituicoes = new ArrayList<Instituicao>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectAllSQL.toString());
            rs = conexao.executeQuery(ps);
            while(rs.next()){
                    instituicoes.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return instituicoes;
    }

    @Override
    protected Instituicao populateObject(ResultSet rs) throws SQLException {
        Instituicao obj = new Instituicao();
        obj.setId(rs.getInt("idInstituicao"));
        obj.setNome(rs.getString("nome"));
        obj.setCodigo(rs.getString("codigo"));
        return obj;
    }   
}