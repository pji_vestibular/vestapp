package com.vestapp.dao;

import com.vestapp.dao.utils.Connect;
import com.vestapp.model.Materia;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MateriaDao extends PatternDao<Materia>{
    
    private StringBuilder insertSQL = new StringBuilder()
            .append("INSERT INTO MATERIA ")
            .append("(nome, conteudo, Concurso_idConcurso) ")
            .append("VALUES ")
            .append("(?,?,?)");
    
    private StringBuilder updateSQL = new StringBuilder()
            .append("UPDATE MATERIA ")
            .append("SET nome=?, conteudo=?, Concurso_idConcurso=? ")
            .append("WHERE idMateria=?");
    
    private StringBuilder deleteSQL = new StringBuilder()
            .append("DELETE FROM MATERIA ")
            .append("WHERE idMateria=?");
    
    private StringBuilder selectIdSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM MATERIA ")
            .append("WHERE idMateria=? ");
	
    private StringBuilder selectAllSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM MATERIA ");
    
    private static MateriaDao instance = new MateriaDao();
    
    private MateriaDao(){
    }
    
    public static MateriaDao getInstance(){
        return instance;
    }
    
    @Override
    public void insert(Materia object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(insertSQL.toString());
            ps.setString(cont++, object.getNome());
            ps.setString(cont++, object.getConteudo());
            ps.setInt(cont++, object.getConcurso().getId());
           
            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void update(Materia object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(updateSQL.toString());
            ps.setString(cont++, object.getNome());
            ps.setString(cont++, object.getConteudo());
            ps.setInt(cont++, object.getConcurso().getId());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void delete(Materia object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(deleteSQL.toString());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public Materia findById(int id) {
        int cont = 1;
        Connect conexao = Connect.getInstance();
        Materia materia = null;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectIdSQL.toString());
            ps.setInt(cont++, id);
            rs = conexao.executeQuery(ps);
            if(rs.next()){
                    materia = populateObject(rs);
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return materia;
    }
    
    private String prepareStringSQLForFilter(Materia filtro){
        StringBuilder sb = new StringBuilder(selectAllSQL.toString());
        sb.append(" WHERE ");

        boolean and = false;

        if(filtro.getNome() != null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" nome = ? ");
        }
        if(filtro.getConteudo()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" conteudo = ? ");
        }
        if(filtro.getConcurso()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" Concurso_idConcurso = ? ");
        }
        
        return sb.toString();
    }
    
    private PreparedStatement prepareStatementForFilter(Materia filtro, PreparedStatement ps) throws SQLException{
        int cont = 1;
        if(filtro.getNome() != null)
            ps.setString(cont++, filtro.getNome());
        if(filtro.getConteudo()!= null)
            ps.setString(cont++, filtro.getConteudo());
        if(filtro.getConcurso()!= null)
            ps.setInt(cont++, filtro.getConcurso().getId());
        
        return ps;
    }

    public List<Materia> findByFilter(Materia filter) {
        Connect conexao = Connect.getInstance();
        List<Materia> materias = new ArrayList<Materia>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(prepareStringSQLForFilter(filter));
            rs = conexao.executeQuery(prepareStatementForFilter(filter, ps));
            while(rs.next()){
                    materias.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return materias;
    }

    @Override
    public List<Materia> findAll() {
        Connect conexao = Connect.getInstance();
        List<Materia> materias = new ArrayList<Materia>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectAllSQL.toString());
            rs = conexao.executeQuery(ps);
            while(rs.next()){
                    materias.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return materias;
    }

    @Override
    protected Materia populateObject(ResultSet rs) throws SQLException {
        Materia obj = new Materia();
        obj.setId(rs.getInt("idInstituicao"));
        obj.setNome(rs.getString("nome"));
        obj.setConteudo(rs.getString("conteudo"));
        obj.setConcurso(ConcursoDao.getInstance().findById(rs.getInt("Concurso_idConcurso")));
        return obj;
    }   
}
