package com.vestapp.dao;

import com.vestapp.dao.utils.Connect;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.vestapp.model.Professor;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class ProfessorDao extends PatternDao<Professor>{

private StringBuilder insertSQL = new StringBuilder()
            .append("INSERT INTO PROFESSOR ")
            .append("(codigo, email_universidade, login, email, nome, data_nascimento, senha) ")
            .append("VALUES ")
            .append("(?,?,?,?,?,?,?)");
    
    private StringBuilder updateSQL = new StringBuilder()
            .append("UPDATE PROFESSOR ")
            .append("SET codigo=?, email_universidade=?, login=?, email=?, nome=?, data_nascimento=?, senha=? ")
            .append("WHERE idProfessor=?");
    
    private StringBuilder deleteSQL = new StringBuilder()
            .append("DELETE FROM PROFESSOR ")
            .append("WHERE idProfessor=?");
    
    private StringBuilder selectIdSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM PROFESSOR ")
            .append("WHERE idProfessor = ? ");
	
    private StringBuilder selectAllSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM PROFESSOR ");
    
    private static ProfessorDao instance = new ProfessorDao();
    
    private ProfessorDao(){
    }
    
    public static ProfessorDao getInstance(){
        return instance;
    }
    
    @Override
    public void insert(Professor object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(insertSQL.toString());
            ps.setString(cont++, object.getCodigo());
            ps.setString(cont++, object.getEmailUniversidade());
            ps.setString(cont++, object.getLogin());
            ps.setString(cont++, object.getEmail());
            ps.setString(cont++, object.getNome());
            ps.setDate(cont++, new java.sql.Date(object.getDataNascimento().getTime()));
            ps.setString(cont++, object.getSenha());
           
            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void update(Professor object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(updateSQL.toString());
            ps.setString(cont++, object.getCodigo());
            ps.setString(cont++, object.getEmailUniversidade());
            ps.setString(cont++, object.getLogin());
            ps.setString(cont++, object.getEmail());
            ps.setString(cont++, object.getNome());
            ps.setDate(cont++, new java.sql.Date(object.getDataNascimento().getTime()));
            ps.setString(cont++, object.getSenha());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void delete(Professor object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(deleteSQL.toString());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public Professor findById(int id) {
        int cont = 1;
        Connect conexao = Connect.getInstance();
        Professor professor = null;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectIdSQL.toString());
            ps.setInt(cont++, id);
            rs = conexao.executeQuery(ps);
            if(rs.next()){
                    professor = populateObject(rs);
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return professor;
    }
    
    private String prepareStringSQLForFilter(Professor filtro){
        StringBuilder sb = new StringBuilder(selectAllSQL.toString());
        sb.append(" WHERE ");

        boolean and = false;

        if(filtro.getCodigo() != null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" codigo = ? ");
        }
        if(filtro.getEmailUniversidade()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" email_universidade = ? ");
        }
        if(filtro.getLogin()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" login = ? ");
        }
        if(filtro.getEmail()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" email = ? ");
        }
        if(filtro.getNome()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" nome = ? ");
        }
        if(filtro.getDataNascimento()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" data_nascimento = ? ");
        }
        
        return sb.toString();
    }
    
    private PreparedStatement prepareStatementForFilter(Professor filtro, PreparedStatement ps) throws SQLException{
        int cont = 1;
        if(filtro.getCodigo() != null)
            ps.setString(cont++, filtro.getCodigo());
        if(filtro.getEmailUniversidade() != null)
            ps.setString(cont++, filtro.getEmailUniversidade());
        if(filtro.getLogin() != null)
            ps.setString(cont++, filtro.getLogin());
        if(filtro.getEmail() != null)
            ps.setString(cont++, filtro.getEmail());
        if(filtro.getNome() != null)
            ps.setString(cont++, filtro.getNome());
        if(filtro.getDataNascimento() != null)
            ps.setDate(cont++, new java.sql.Date(filtro.getDataNascimento().getTime()));
        
        return ps;
    }

    public List<Professor> findByFilter(Professor filter) {
        Connect conexao = Connect.getInstance();
        List<Professor> professores = new ArrayList<Professor>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(prepareStringSQLForFilter(filter));
            rs = conexao.executeQuery(prepareStatementForFilter(filter, ps));
            while(rs.next()){
                    professores.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return professores;
    }

    @Override
    public List<Professor> findAll() {
        Connect conexao = Connect.getInstance();
        List<Professor> professores = new ArrayList<Professor>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectAllSQL.toString());
            rs = conexao.executeQuery(ps);
            while(rs.next()){
                    professores.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return professores;
    }

    @Override
    protected Professor populateObject(ResultSet rs) throws SQLException {
        Professor obj = new Professor();
        obj.setId(rs.getInt("idProfessor"));
        obj.setCodigo(rs.getString("codigo"));
        obj.setEmailUniversidade(rs.getString("email_universidade"));
        obj.setLogin(rs.getString("login"));
        obj.setEmail(rs.getString("email"));
        obj.setNome(rs.getString("nome"));
        obj.setDataNascimento(rs.getDate("data_nascimento"));
        obj.setSenha(rs.getString("senha"));
        return obj;
    }   
    
}
