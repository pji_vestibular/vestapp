package com.vestapp.dao;

import com.vestapp.dao.utils.Connect;
import com.vestapp.model.Resumo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResumoDao extends PatternDao<Resumo>{
    
    private StringBuilder insertSQL = new StringBuilder()
            .append("INSERT INTO RESUMO ")
            .append("(nome, data, texto, Materia_idMateria, Professor_idProfessor) ")
            .append("VALUES ")
            .append("(?,?,?,?,?)");
    
    private StringBuilder updateSQL = new StringBuilder()
            .append("UPDATE RESUMO ")
            .append("SET nome=?, data=?, texto=?, Materia_idMateria=?, Professor_idProfessor=? ")
            .append("WHERE idResumo=?");
    
    private StringBuilder deleteSQL = new StringBuilder()
            .append("DELETE FROM RESUMO ")
            .append("WHERE idResumo=?");
    
    private StringBuilder selectIdSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM RESUMO ")
            .append("WHERE idResumo = ? ");
	
    private StringBuilder selectAllSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM RESUMO ");
    
    private static ResumoDao instance = new ResumoDao();
    
    private ResumoDao(){
    }
    
    public static ResumoDao getInstance(){
        return instance;
    }
    
    @Override
    public void insert(Resumo object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(insertSQL.toString());
            ps.setString(cont++, object.getNome());
            ps.setString(cont++, object.getTexto());
            ps.setDate(cont++, new java.sql.Date(object.getData().getTime()));
            ps.setInt(cont++, object.getMateria().getId());
            ps.setInt(cont++, object.getProfessor().getId());
           
            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void update(Resumo object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(updateSQL.toString());
            ps.setString(cont++, object.getNome());
            ps.setString(cont++, object.getTexto());
            ps.setDate(cont++, new java.sql.Date(object.getData().getTime()));
            ps.setInt(cont++, object.getMateria().getId());
            ps.setInt(cont++, object.getProfessor().getId());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void delete(Resumo object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(deleteSQL.toString());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public Resumo findById(int id) {
        int cont = 1;
        Connect conexao = Connect.getInstance();
        Resumo resumo = null;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectIdSQL.toString());
            ps.setInt(cont++, id);
            rs = conexao.executeQuery(ps);
            if(rs.next()){
                    resumo = populateObject(rs);
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return resumo;
    }
    
    private String prepareStringSQLForFilter(Resumo filtro){
        StringBuilder sb = new StringBuilder(selectAllSQL.toString());
        sb.append(" WHERE ");

        boolean and = false;

        if(filtro.getNome() != null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" nome = ? ");
        }
        if(filtro.getData()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" data = ? ");
        }
        if(filtro.getMateria()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" Materia_idMateria = ? ");
        }
        if(filtro.getProfessor()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" Professor_idProfessor = ? ");
        }
        return sb.toString();
    }
    
    private PreparedStatement prepareStatementForFilter(Resumo filtro, PreparedStatement ps) throws SQLException{
        int cont = 1;
        if(filtro.getNome() != null)
            ps.setString(cont++, filtro.getNome());
        if(filtro.getData()!= null)
            ps.setDate(cont++, new java.sql.Date(filtro.getData().getTime()));
        if(filtro.getMateria()!= null)
            ps.setInt(cont++, filtro.getMateria().getId());
        if(filtro.getProfessor()!= null) 
            ps.setInt(cont++, filtro.getProfessor().getId());
        
        return ps;
    }

    public List<Resumo> findByFilter(Resumo filter) {
        Connect conexao = Connect.getInstance();
        List<Resumo> resumos = new ArrayList<Resumo>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(prepareStringSQLForFilter(filter));
            rs = conexao.executeQuery(prepareStatementForFilter(filter, ps));
            while(rs.next()){
                    resumos.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return resumos;
    }

    @Override
    public List<Resumo> findAll() {
        Connect conexao = Connect.getInstance();
        List<Resumo> resumos = new ArrayList<Resumo>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectAllSQL.toString());
            rs = conexao.executeQuery(ps);
            while(rs.next()){
                    resumos.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return resumos;
    }

    @Override
    protected Resumo populateObject(ResultSet rs) throws SQLException {
        Resumo obj = new Resumo();
        obj.setId(rs.getInt("idResumo"));
        obj.setNome(rs.getString("nome"));
        obj.setTexto(rs.getString("texto"));
        obj.setData(rs.getDate("data"));
        obj.setMateria(MateriaDao.getInstance().findById(rs.getInt("Materia_idMateria")));
        obj.setProfessor(ProfessorDao.getInstance().findById(rs.getInt("Professor_idProfessor")));
        return obj;
    }   
}
