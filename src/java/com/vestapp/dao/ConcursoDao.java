package com.vestapp.dao;

import com.vestapp.dao.utils.Connect;
import com.vestapp.model.Concurso;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConcursoDao extends PatternDao<Concurso>{

    private StringBuilder insertSQL = new StringBuilder()
            .append("INSERT INTO CONCURSO ")
            .append("(data, valor, Instituicao_idInstituicao, nome) ")
            .append("VALUES ")
            .append("(?,?,?,?)");
    
    private StringBuilder updateSQL = new StringBuilder()
            .append("UPDATE CONCURSO ")
            .append("SET data=?, valor=?, Instituicao_idInstituicao=?, nome=? ")
            .append("WHERE idConcurso=?");
    
    private StringBuilder deleteSQL = new StringBuilder()
            .append("DELETE FROM CONCURSO ")
            .append("WHERE idConcurso=?");
    
    private StringBuilder selectIdSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM CONCURSO ")
            .append("WHERE idConcurso = ? ");
	
    private StringBuilder selectAllSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM CONCURSO ");
    
    private static ConcursoDao instance = new ConcursoDao();
    
    private ConcursoDao(){
    }
    
    public static ConcursoDao getInstance(){
        return instance;
    }
    
    @Override
    public void insert(Concurso object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(insertSQL.toString());
            ps.setDate(cont++, new java.sql.Date(object.getData().getTime()));
            ps.setDouble(cont++, object.getValor());
            ps.setDouble(cont++, object.getValor());
            ps.setInt(cont++, object.getInstituicao().getId());
            ps.setString(cont++, object.getNome());
           
            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void update(Concurso object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(updateSQL.toString());
            ps.setDate(cont++, new java.sql.Date(object.getData().getTime()));
            ps.setDouble(cont++, object.getValor());
            ps.setDouble(cont++, object.getValor());
            ps.setInt(cont++, object.getInstituicao().getId());
            ps.setString(cont++, object.getNome());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void delete(Concurso object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(deleteSQL.toString());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public Concurso findById(int id) {
        int cont = 1;
        Connect conexao = Connect.getInstance();
        Concurso concurso = null;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectIdSQL.toString());
            ps.setInt(cont++, id);
            rs = conexao.executeQuery(ps);
            if(rs.next()){
                    concurso = populateObject(rs);
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return concurso;
    }
    
    private String prepareStringSQLForFilter(Concurso filtro){
        StringBuilder sb = new StringBuilder(selectAllSQL.toString());
        sb.append(" WHERE ");

        boolean and = false;

        if(filtro.getData() != null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" data = ? ");
        }
        if(filtro.getValor()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" valor = ? ");
        }
        if(filtro.getInstituicao()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" Instituicao_idInstituicao = ? ");
        }
        if(filtro.getNome()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" nome = ? ");
        }
        return sb.toString();
    }
    
    private PreparedStatement prepareStatementForFilter(Concurso filtro, PreparedStatement ps) throws SQLException{
        int cont = 1;
        if(filtro.getData() != null)
            ps.setDate(cont++, new java.sql.Date(filtro.getData().getTime()));
        if(filtro.getValor()!= null)
            ps.setDouble(cont++, filtro.getValor());
        if(filtro.getInstituicao()!= null)
            ps.setInt(cont++, filtro.getInstituicao().getId());
        if(filtro.getNome()!= null)
            ps.setString(cont++, filtro.getNome());
        
        return ps;
    }

    public List<Concurso> findByFilter(Concurso filter) {
        Connect conexao = Connect.getInstance();
        List<Concurso> concursos = new ArrayList<Concurso>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(prepareStringSQLForFilter(filter));
            rs = conexao.executeQuery(prepareStatementForFilter(filter, ps));
            while(rs.next()){
                    concursos.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return concursos;
    }

    @Override
    public List<Concurso> findAll() {
        Connect conexao = Connect.getInstance();
        List<Concurso> concursos = new ArrayList<Concurso>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectAllSQL.toString());
            rs = conexao.executeQuery(ps);
            while(rs.next()){
                    concursos.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return concursos;
    }

    @Override
    protected Concurso populateObject(ResultSet rs) throws SQLException {
        Concurso obj = new Concurso();
        obj.setId(rs.getInt("idConcurso"));
        obj.setData(rs.getDate("data"));
        obj.setValor(rs.getDouble("valor"));
        obj.setInstituicao(InstituicaoDao.getInstance().findById(rs.getInt("Instituicao_idInstituicao")));
        obj.setNome(rs.getString("nome"));
        return obj;
    }   
    
}
