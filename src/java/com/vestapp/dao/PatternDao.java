package com.vestapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Alexandre
 */
public abstract class PatternDao<T> {
    
    	public abstract void insert(T object);
	public abstract void update(T object);
	public abstract void delete(T object);

	public abstract T findById(int id);
	public abstract List<T> findByFilter(T filter);	
	public abstract List<T> findAll();
	
	protected abstract T populateObject(ResultSet rs) throws SQLException;
}
