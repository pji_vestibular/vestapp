package com.vestapp.dao;

import com.vestapp.dao.utils.Connect;
import com.vestapp.model.Estudante;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EstudanteDao extends PatternDao<Estudante>{
    
    private StringBuilder insertSQL = new StringBuilder()
            .append("INSERT INTO ESTUDANTE ")
            .append("(nome_colegio, numero_matricula, login, email, nome, data_nascimento, senha) ")
            .append("VALUES ")
            .append("(?,?,?,?,?,?,?)");
    
    private StringBuilder updateSQL = new StringBuilder()
            .append("UPDATE ESTUDANTE ")
            .append("SET nome_colegio=?, numero_matricula=?, login=?, email=?, nome=?, data_nascimento=?, senha=? ")
            .append("WHERE idEstudante=?");
    
    private StringBuilder deleteSQL = new StringBuilder()
            .append("DELETE FROM ESTUDANTE ")
            .append("WHERE idEstudante=?");
    
    private StringBuilder selectIdSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM ESTUDANTE ")
            .append("WHERE idEstudante = ? ");
	
    private StringBuilder selectAllSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM ESTUDANTE ");
    
    private static EstudanteDao instance = new EstudanteDao();
    
    private EstudanteDao(){
    }
    
    public static EstudanteDao getInstance(){
        return instance;
    }
    
    @Override
    public void insert(Estudante object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(insertSQL.toString());
            ps.setString(cont++, object.getNomeColegio());
            ps.setString(cont++, object.getNumeroMatricula());
            ps.setString(cont++, object.getLogin());
            ps.setString(cont++, object.getEmail());
            ps.setString(cont++, object.getNome());
            ps.setDate(cont++, new java.sql.Date(object.getDataNascimento().getTime()));
            ps.setString(cont++, object.getSenha());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void update(Estudante object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(updateSQL.toString());
            ps.setString(cont++, object.getNomeColegio());
            ps.setString(cont++, object.getNumeroMatricula());
            ps.setString(cont++, object.getLogin());
            ps.setString(cont++, object.getEmail());
            ps.setString(cont++, object.getNome());
            ps.setDate(cont++, new java.sql.Date(object.getDataNascimento().getTime()));
            ps.setString(cont++, object.getSenha());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void delete(Estudante object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(deleteSQL.toString());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public Estudante findById(int id) {
        int cont = 1;
        Connect conexao = Connect.getInstance();
        Estudante estudante = null;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectIdSQL.toString());
            ps.setInt(cont++, id);
            rs = conexao.executeQuery(ps);
            if(rs.next()){
                    estudante = populateObject(rs);
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return estudante;
    }
    
    private String prepareStringSQLForFilter(Estudante filtro){
        StringBuilder sb = new StringBuilder(selectAllSQL.toString());
        sb.append(" WHERE ");

        boolean and = false;

        if(filtro.getNome() != null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" nome = ? ");
        }
        if(filtro.getNomeColegio()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" nome_colegio = ? ");
        }
        if(filtro.getNumeroMatricula()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" numero_matricula = ? ");
        }
        if(filtro.getLogin()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" login = ? ");
        }
        if(filtro.getEmail()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" email = ? ");
        }
        if(filtro.getDataNascimento()!= null){
            if(!and){
                    and = true;
            }else{
                        sb.append(" AND ");
            }
            sb.append(" data_nascimento = ? ");
        }
        return sb.toString();
    }
    
    private PreparedStatement prepareStatementForFilter(Estudante filtro, PreparedStatement ps) throws SQLException{
        int cont = 1;
        if(filtro.getNome() != null)
            ps.setString(cont++, filtro.getNome());
        if(filtro.getNomeColegio()!= null)
            ps.setString(cont++, filtro.getNomeColegio());
        if(filtro.getNumeroMatricula()!= null)
            ps.setString(cont++, filtro.getNumeroMatricula());
        if(filtro.getLogin()!= null)
            ps.setString(cont++, filtro.getLogin());
        if(filtro.getEmail()!= null)
            ps.setString(cont++, filtro.getEmail());
        if(filtro.getDataNascimento()!= null)
            ps.setDate(cont++, new java.sql.Date(filtro.getDataNascimento().getTime()));
        
        return ps;
    }

    @Override
    public List<Estudante> findByFilter(Estudante filter) {
        Connect conexao = Connect.getInstance();
        List<Estudante> estudantes = new ArrayList<Estudante>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(prepareStringSQLForFilter(filter));
            rs = conexao.executeQuery(prepareStatementForFilter(filter, ps));
            while(rs.next()){
                    estudantes.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return estudantes;
    }
  
    @Override
    public List<Estudante> findAll() {
        Connect conexao = Connect.getInstance();
        List<Estudante> estudantes = new ArrayList<Estudante>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectAllSQL.toString());
            rs = conexao.executeQuery(ps);
            while(rs.next()){
                    estudantes.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return estudantes;
    }

    @Override
    protected Estudante populateObject(ResultSet rs) throws SQLException {
        Estudante obj = new Estudante();
        obj.setId(rs.getInt("idEstudante"));
        obj.setNomeColegio(rs.getString("nome_colegio"));
        obj.setNumeroMatricula(rs.getString("numero_matricula"));
        obj.setLogin(rs.getString("login"));
        obj.setEmail(rs.getString("email"));
        obj.setNome(rs.getString("nome"));
        obj.setDataNascimento(rs.getDate("data_nascimento"));
        obj.setSenha(rs.getString("senha"));
        return obj;
    }   
}