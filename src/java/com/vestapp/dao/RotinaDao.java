package com.vestapp.dao;

import com.vestapp.dao.utils.Connect;
import com.vestapp.model.Rotina;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RotinaDao extends PatternDao<Rotina>{
    
    private StringBuilder insertSQL = new StringBuilder()
            .append("INSERT INTO ROTINA ")
            .append("(data_inicial, data_final, descricao, Concurso_idConcurso, Estudante_idEstudante) ")
            .append("VALUES ")
            .append("(?,?,?,?,?)");
    
    private StringBuilder updateSQL = new StringBuilder()
            .append("UPDATE ROTINA ")
            .append("SET data_inicial=?, data_final=?, descricao=?, Concurso_idConcurso=?, Estudante_idEstudante=? ")
            .append("WHERE idRotina=?");
    
    private StringBuilder deleteSQL = new StringBuilder()
            .append("DELETE FROM ROTINA ")
            .append("WHERE idRotina=?");
    
    private StringBuilder selectIdSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM ROTINA ")
            .append("WHERE idRotina = ? ");
	
    private StringBuilder selectAllSQL =  new StringBuilder()
            .append("SELECT * ")
            .append("FROM ROTINA ");
    
    private static RotinaDao instance = new RotinaDao();
    
    private RotinaDao(){
    }
    
    public static RotinaDao getInstance(){
        return instance;
    }
    
    @Override
    public void insert(Rotina object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(insertSQL.toString());
            ps.setDate(cont++, new java.sql.Date(object.getDataInicio().getTime()));
            ps.setDate(cont++, new java.sql.Date(object.getDataFinal().getTime()));
            ps.setString(cont++, object.getDescricao());
            ps.setInt(cont++, object.getConcurso().getId());
            ps.setInt(cont++, object.getEstudante().getId());
           
            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void update(Rotina object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(updateSQL.toString());
            ps.setDate(cont++, new java.sql.Date(object.getDataInicio().getTime()));
            ps.setDate(cont++, new java.sql.Date(object.getDataFinal().getTime()));
            ps.setString(cont++, object.getDescricao());
            ps.setInt(cont++, object.getConcurso().getId());
            ps.setInt(cont++, object.getEstudante().getId());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public void delete(Rotina object) {
        Connect conexao = Connect.getInstance();      
        int cont = 1;
        PreparedStatement ps;
        try {
            ps = conexao.getConnection().prepareStatement(deleteSQL.toString());
            ps.setInt(cont++, object.getId());

            conexao.executeUpdate(ps);
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde "+ e.toString());
        }
    }

    @Override
    public Rotina findById(int id) {
        int cont = 1;
        Connect conexao = Connect.getInstance();
        Rotina rotina = null;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectIdSQL.toString());
            ps.setInt(cont++, id);
            rs = conexao.executeQuery(ps);
            if(rs.next()){
                    rotina = populateObject(rs);
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return rotina;
    }
    
    private String prepareStringSQLForFilter(Rotina filtro){
        StringBuilder sb = new StringBuilder(selectAllSQL.toString());
        sb.append(" WHERE ");

        boolean and = false;

        if(filtro.getDataInicio() != null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" data_inicial = ? ");
        }
        if(filtro.getDataFinal()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" data_final = ? ");
        }
        if(filtro.getDescricao()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" descricao = ? ");
        }
        if(filtro.getConcurso()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" Concurso_idConcurso = ? ");
        }
        if(filtro.getEstudante()!= null){
            if(!and){
                    and = true;
            }else{
                    sb.append(" AND ");
            }
            sb.append(" Estudante_idEstudante = ? ");
        }
        return sb.toString();
    }
    
    private PreparedStatement prepareStatementForFilter(Rotina filtro, PreparedStatement ps) throws SQLException{
        int cont = 1;
        if(filtro.getDataInicio()!= null)
            ps.setDate(cont++, new java.sql.Date(filtro.getDataInicio().getTime()));
        if(filtro.getDataFinal()!= null)
            ps.setDate(cont++, new java.sql.Date(filtro.getDataFinal().getTime()));
        if(filtro.getDescricao()!= null)
            ps.setString(cont++, filtro.getDescricao());
        if(filtro.getConcurso()!= null)
            ps.setInt(cont++, filtro.getConcurso().getId());
        if(filtro.getDataFinal()!= null)
            ps.setInt(cont++, filtro.getEstudante().getId());
        
        return ps;
    }

    public List<Rotina> findByFilter(Rotina filter) {
        Connect conexao = Connect.getInstance();
        List<Rotina> rotinas = new ArrayList<Rotina>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(prepareStringSQLForFilter(filter));
            rs = conexao.executeQuery(prepareStatementForFilter(filter, ps));
            while(rs.next()){
                    rotinas.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return rotinas;
    }

    @Override
    public List<Rotina> findAll() {
        Connect conexao = Connect.getInstance();
        List<Rotina> rotinas = new ArrayList<Rotina>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conexao.getConnection().prepareStatement(selectAllSQL.toString());
            rs = conexao.executeQuery(ps);
            while(rs.next()){
                    rotinas.add(populateObject(rs));
            }
        } catch (SQLException e) {
                throw new RuntimeException("Problemas no sistema, por favor tente mais tarde" + e.toString());
        }
        return rotinas;
    }

    @Override
    protected Rotina populateObject(ResultSet rs) throws SQLException {
        Rotina obj = new Rotina();
        obj.setId(rs.getInt("idRotina"));
        obj.setDataInicio(rs.getDate("data_inicial"));
        obj.setDataFinal(rs.getDate("data_final"));
        obj.setDescricao(rs.getString("descricao"));
        obj.setConcurso(ConcursoDao.getInstance().findById(rs.getInt("Concurso_idConcurso")));
        obj.setEstudante(EstudanteDao.getInstance().findById(rs.getInt("Estudante_idEstudante")));
        return obj;
    }   
}
