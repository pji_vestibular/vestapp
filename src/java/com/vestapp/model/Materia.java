package com.vestapp.model;

/**
 *
 * @author Alexandre
 */
public class Materia {

    private Integer id;
    private String nome;
    private String conteudo;
    private Concurso concurso;

    public Materia() {
    }

    public Materia(Integer id, String nome, String conteudo, Concurso concurso) {
        this.id = id;
        this.nome = nome;
        this.conteudo = conteudo;
        this.concurso = concurso;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public Concurso getConcurso() {
        return concurso;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }

    @Override
    public String toString() {
        return "Materia{" + "id=" + id + ", nome=" + nome + ", conteudo=" + conteudo + ", concurso=" + concurso + '}';
    }
    
    

    
}
