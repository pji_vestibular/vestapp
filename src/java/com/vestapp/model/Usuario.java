package com.vestapp.model;

import java.io.Serializable;
import java.sql.Date;


/**
 *
 * @author Alexandre
 */
public abstract class Usuario implements Serializable{
 
    private Integer id;
    private String login;
    private String email;
    private String nome;
    private String senha;
    private Date dataNascimento;

    public Usuario() {
    }

    public Usuario(Integer id, String login, String email, String nome, String senha, Date dataNascimento) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.nome = nome;
        this.senha = senha;
        this.dataNascimento = dataNascimento;
    }
    
    public Usuario(String login, String email, String nome, String senha, Date dataNascimento) {
        this.login = login;
        this.email = email;
        this.nome = nome;
        this.senha = senha;
        this.dataNascimento = dataNascimento;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", login=" + login + ", email=" + email + ", nome=" + nome + ", dataNascimento=" + dataNascimento + '}';
    }
}
