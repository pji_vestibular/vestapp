package com.vestapp.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * @author Alexandre
 */
public class Concurso {
    
    private Integer id;
    private Date data;
    private Double valor;
    private String nome;
    private Instituicao instituicao;
    private List<Materia> materias;
    private List<Rotina> rotinas; 
    
    
    public Concurso() {
        this.materias = new ArrayList<Materia>();
        this.rotinas = new ArrayList<Rotina>();
    }

    public Concurso(Integer id, Date data, Double valor, String nome, Instituicao instituicao) {
        this.id = id;
        this.data = data;
        this.valor = valor;
        this.nome = nome;
        this.instituicao = instituicao;
        this.materias = new ArrayList<Materia>();
        this.rotinas = new ArrayList<Rotina>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public List<Materia> getMaterias() {
        return materias;
    }

    public void setMaterias(List<Materia> materias) {
        this.materias = materias;
    }

    public List<Rotina> getRotinas() {
        return rotinas;
    }

    public void setRotinas(List<Rotina> rotinas) {
        this.rotinas = rotinas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Concurso{" + "id=" + id + ", data=" + data + ", valor=" + valor + ", instituicao=" + instituicao + '}';
    }
    
}
