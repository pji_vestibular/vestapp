package com.vestapp.model;

import java.util.Date;

/**
 *
 * @author Alexandre
 */
public class Resumo {
    
    private Integer id;
    private String nome;
    private String texto;
    private Date data;
    private Materia materia;
    private Professor professor;

    public Resumo(Integer id, String nome, String texto, Date data, Materia materia, Professor professor) {
        this.id = id;
        this.nome = nome;
        this.texto = texto;
        this.data = data;
        this.materia = materia;
        this.professor = professor;
    }

    public Resumo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @Override
    public String toString() {
        return "Resumo{" + "id=" + id + ", nome=" + nome + ", data=" + data + ", materia=" + materia + ", professor=" + professor + '}';
    }
}
