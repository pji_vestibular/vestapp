/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vestapp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Alexandre
 */
public class Estudante extends Usuario{
    
    private String numeroMatricula;
    private String nomeColegio;
    private List<Rotina> rotina;

    public Estudante() {
        super();
        rotina = new ArrayList<Rotina>();
    }
    
    public Estudante(Integer id, String numeroMatricula, String nomeColegio, String login, String email, String nome, String senha, Date dataNascimento) {
        super(id, login, email, nome, senha, dataNascimento);
        this.numeroMatricula = numeroMatricula;
        this.nomeColegio = nomeColegio;
        this.rotina = new ArrayList<Rotina>();
    }

    public String getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(String numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }

    public String getNomeColegio() {
        return nomeColegio;
    }

    public void setNomeColegio(String nomeColegio) {
        this.nomeColegio = nomeColegio;
    }

    public List<Rotina> getRotina() {
        return rotina;
    }

    public void setRotina(List<Rotina> rotina) {
        this.rotina = rotina;
    }
    
    @Override
    public String toString() {
        return "Estudante{" + "numeroMatricula=" + numeroMatricula + ", nomeColegio=" + nomeColegio + '}';
    }
    
}
