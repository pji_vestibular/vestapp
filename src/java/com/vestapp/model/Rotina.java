package com.vestapp.model;

import java.util.Date;

/**
 * @author Alexandre
 */
public class Rotina {
    
    private Integer id;
    private Date dataInicio;
    private Date dataFinal;
    private String descricao;
    private Concurso concurso;
    private Estudante estudante;

    public Rotina(Integer id, Date dataInicio, Date dataFinal, String descricao, Concurso concurso, Estudante estudante) {
        this.id = id;
        this.dataInicio = dataInicio;
        this.dataFinal = dataFinal;
        this.descricao = descricao;
        this.concurso = concurso;
        this.estudante = estudante;
    }

    public Rotina() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Concurso getConcurso() {
        return concurso;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }

    public Estudante getEstudante() {
        return estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    @Override
    public String toString() {
        return "Rotina{" + "id=" + id + ", dataInicio=" + dataInicio + ", dataFinal=" + dataFinal + ", descri\u00e7\u00e3o=" + descricao + ", concurso=" + concurso + ", estudante=" + estudante + '}';
    }
}
