package com.vestapp.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alexandre
 */
public class Professor extends Usuario{

    private String codigo;
    private String emailUniversidade;
    private List<Resumo> resumo;
    
    public Professor() {
        super();
        this.resumo = new ArrayList<Resumo>();
    }  

    public Professor(String codigo, String emailUniversidade, Integer id, String login, String email, String nome, String senha, Date dataNascimento) {
        super(id, login, email, nome, senha, dataNascimento);
        this.codigo = codigo;
        this.emailUniversidade = emailUniversidade;
        this.resumo = new ArrayList<Resumo>();
    }    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEmailUniversidade() {
        return emailUniversidade;
    }

    public void setEmailUniversidade(String emailUniversidade) {
        this.emailUniversidade = emailUniversidade;
    }

    public List<Resumo> getResumo() {
        return resumo;
    }

    public void setResumo(List<Resumo> resumo) {
        this.resumo = resumo;
    }

    
    
    @Override
    public String toString() {
        return "Professor{" + "codigo=" + codigo + ", emailUniversidade=" + emailUniversidade + '}';
    }
}
