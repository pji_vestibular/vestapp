package com.vestapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexandre
 */
public class Instituicao {
    
    private Integer id;
    private String nome;
    private String codigo;
    private List<Materia> materia;

    public Instituicao() {
        this.materia = new ArrayList<Materia>();
    }

    public Instituicao(Integer id, String nome, String codigo) {
        this.id = id;
        this.nome = nome;
        this.codigo = codigo;
        this.materia = new ArrayList<Materia>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<Materia> getConcursos() {
        return materia;
    }

    public void setConcursos(List<Materia> materia) {
        this.materia = materia;
    }

    @Override
    public String toString() {
        return "Instituicao{" + "id=" + id + ", nome=" + nome + ", codigo=" + codigo + '}';
    }
}
