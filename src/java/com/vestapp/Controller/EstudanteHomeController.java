/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vestapp.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Alexandre
 */
public class EstudanteHomeController extends HttpServlet {
    
    protected static final String ESTUDANTE_LOGOUT = "/estudante/sair";
    
    protected static final String ESTUDANTE_HOME = "/WEB-INF/pages/EstudanteView.jsp";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            String url = request.getServletPath();
            switch (url){
                case ESTUDANTE_LOGOUT:
                    this.logOut(request, response);
                break;
                default:
                     RequestDispatcher rd;
                    rd = request.getRequestDispatcher(ESTUDANTE_HOME);
                    rd.forward(request, response);
                break;
            }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
    
    
    protected void logOut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
                    HttpSession session = request.getSession();

                    session.removeAttribute("e");
                    session.invalidate();

                    response.sendRedirect(request.getContextPath() + "/login");
    }

}
