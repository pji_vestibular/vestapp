package com.vestapp.Controller;

import com.vestapp.bc.EstudanteBc;
import com.vestapp.model.Estudante;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Alexandre
 */

public class LoginController extends HttpServlet {

    protected static final String LOGIN_VIEW = "/WEB-INF/pages/LoginView.jsp";
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd;
        rd = request.getRequestDispatcher(LOGIN_VIEW);
        rd.forward(request, response);

    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Estudante estudante = new Estudante();
            Estudante estudanteLogado;
            
            estudante.setSenha(request.getParameter("senha"));
            estudante.setLogin(request.getParameter("login"));

            estudanteLogado = EstudanteBc.getInstance().login(estudante);
            	
            if(estudanteLogado != null){
                    HttpSession session = request.getSession();
                    estudanteLogado.setSenha(null);
                    session.setAttribute("e", estudanteLogado);
                    response.sendRedirect("estudante");
            }else{
                    request.setAttribute("msg", showErrorDialog("Não encontrado"));
                    request.getRequestDispatcher(LOGIN_VIEW).forward(request, response);
            }
            
        } catch (Exception e) {
            request.setAttribute("msg", showErrorDialog(e.getMessage()));
            request.getRequestDispatcher(LOGIN_VIEW).forward(request, response);
        }
    }
    
    public String showErrorDialog(String mensagem){
        return "<div class='alert alert-danger alert-dismissible col-xs-6 col-xs-offset-3'>"
                + "<span class='glyphicon glyphicon-alert'></span>&nbsp;&nbsp;"
                + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"
                    + "<span aria-hidden='true'>&times;</span>"
                + "</button>"
                + mensagem
            + "</div>";
    }
}