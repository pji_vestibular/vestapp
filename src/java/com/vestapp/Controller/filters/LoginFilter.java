package com.vestapp.Controller.filters;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(
		dispatcherTypes = {
				DispatcherType.REQUEST, 
				DispatcherType.FORWARD, 
				DispatcherType.INCLUDE, 
				DispatcherType.ERROR
		}
					, 
		urlPatterns = { 
				"/LoginFilter", 
				"/estudante/*",
                                "/professor/*"
		}, 
		servletNames = { "EstudanteHomeController" })
public class LoginFilter implements Filter {


    public LoginFilter() {

    }


    @Override
	public void destroy() {

	}

    @Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		HttpSession session = req.getSession();
		
		if(session.getAttribute("e") == null && session.getAttribute("p") == null){
			res.sendRedirect(req.getContextPath() + "/login");
		}else{
			chain.doFilter(request, response);			
		}
		
	}

    /**
     *
     * @param fConfig
     * @throws ServletException
     */
    @Override
	public void init(FilterConfig fConfig) throws ServletException {

	}

}
