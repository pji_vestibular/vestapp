package com.vestapp.bc;

import com.vestapp.bc.utils.Validacoes;
import com.vestapp.dao.ProfessorDao;
import com.vestapp.model.Professor;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class ProfessorBc extends PatternBc<Professor>{
    
    //sigleton
    private static final ProfessorBc instance = new ProfessorBc();
    
    private ProfessorBc() {
    }

    public static ProfessorBc getInstance() {
        return instance;
    }

    @Override
    public void insert(Professor objeto) {
        validateObject(objeto);
        validateExistingUser(objeto);
        ProfessorDao.getInstance().insert(objeto);
    }

    @Override
    public void update(Professor objeto) {
        validateObject(objeto);
        ProfessorDao.getInstance().update(objeto);
    }

    @Override
    public void delete(Professor objeto) {
        if(findById(objeto.getId())!=null){
            ProfessorDao.getInstance().delete(objeto);
        }
    }

    @Override
    public List<Professor> findAll() {
        return ProfessorDao.getInstance().findAll();
    }

    @Override
    public Professor findById(int id) {
        return ProfessorDao.getInstance().findById(id);
    }
    
    @Override
    public List<Professor> findByFilter(Professor filter) {
        if(!validateFilter(filter)){
            throw new RuntimeException("É necessario preencher ao menos um campo de busca");
        }
        return ProfessorDao.getInstance().findByFilter(filter);
    }

    @Override
    protected void validateObject(Professor object) {
        if(object == null){
            throw new RuntimeException("Professor nulo");
        }
        if(StringUtils.isBlank(object.getCodigo())){
            throw new RuntimeException("Código nulo");
        }
        if(StringUtils.isBlank(object.getEmailUniversidade())){
            throw new RuntimeException("Email da universidade em branco");
        }
        if(!Validacoes.ValidarEmail(object.getEmailUniversidade())){
            throw new RuntimeException("Email da universidade inválido");
        }
        if(StringUtils.isBlank(object.getLogin())){
            throw new RuntimeException("Login em branco");
        }
        if(StringUtils.isBlank(object.getEmail())){
            throw new RuntimeException("Email em branco");
        }
        if(!Validacoes.ValidarEmail(object.getEmail())){
            throw new RuntimeException("Email inválido");
        }
        if(StringUtils.isBlank(object.getNome())){
            throw new RuntimeException("Nome em branco");
        }
        if(object.getDataNascimento()==null){
            throw new RuntimeException("Data de nascimento em branco");
        }
        if(object.getDataNascimento().after(new Date())){
            throw new RuntimeException("Data de nascimento inválida");
        }
        if(StringUtils.isBlank(object.getSenha())){
            throw new RuntimeException("Senha em branco");
        }
        if(object.getSenha().trim().length() < 6){
            throw new RuntimeException("Senha muito curta");
        }
    }

    @Override
    protected boolean validateFilter(Professor object) {
        return object != null && (StringUtils.isNotBlank(object.getCodigo()) || 
                StringUtils.isNotBlank(object.getEmailUniversidade()) ||
                StringUtils.isNotBlank(object.getLogin()) ||
                StringUtils.isNotBlank(object.getEmail()) ||
                StringUtils.isNotBlank(object.getNome()) ||
                object.getDataNascimento() != null
            );
    }
    
    protected void validateExistingUser(Professor object){
        Professor filtro = new Professor();
        filtro.setLogin(object.getLogin());
        if(findByFilter(filtro).size()> 0){
            throw new RuntimeException("Login já utilizado");
        }
        filtro.setLogin(null);
        filtro.setEmail(object.getEmail());
        if(findByFilter(filtro).size()> 0){
            throw new RuntimeException("Email já cadastrado");
        }
    }
}
