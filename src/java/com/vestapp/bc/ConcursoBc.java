package com.vestapp.bc;

import com.vestapp.dao.ConcursoDao;
import com.vestapp.model.Concurso;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class ConcursoBc extends PatternBc<Concurso>{

    private static final ConcursoBc instance = new ConcursoBc();
    
    private ConcursoBc(){
    }
    
    public static ConcursoBc getInstance(){
        return instance;
    }

    @Override
    public void insert(Concurso objeto) {
        validateObject(objeto);
        ConcursoDao.getInstance().insert(objeto);
    }

    @Override
    public void update(Concurso objeto) {
        validateObject(objeto);
        ConcursoDao.getInstance().update(objeto);
    }

    @Override
    public void delete(Concurso objeto) {
        if(findById(objeto.getId())!=null){
            ConcursoDao.getInstance().delete(objeto);
        }
    }

    @Override
    public List<Concurso> findAll() {
        return ConcursoDao.getInstance().findAll();
    }

    @Override
    public Concurso findById(int id) {
        return ConcursoDao.getInstance().findById(id);
    }

    @Override
    public List<Concurso> findByFilter(Concurso filter) {
        if(!validateFilter(filter)){
            throw new RuntimeException("É necessario preencher ao menos um campo de busca");
        }
        return ConcursoDao.getInstance().findByFilter(filter);
    }

    @Override
    protected void validateObject(Concurso object) {
        if(object == null){
            throw new RuntimeException("Concurso nulo");
        }
        if(object.getData() == null){
            throw new RuntimeException("Data da prova nula");
        }
        if(object.getData().before(new Date())){
            throw new RuntimeException("Data da prova inválida");
        }
        if(object.getValor() == null){
            throw new RuntimeException("Valor nulo");
        }
        if(object.getValor() < 0){
            throw new RuntimeException("Valor inválido");
        }
        if(StringUtils.isBlank(object.getNome())){
            throw new RuntimeException("Nome nulo");
        }
    }

    @Override
    protected boolean validateFilter(Concurso object) {
        return object != null && (object.getData()!=null ||
                (object.getValor()!=null && object.getValor()>0) ||
                (object.getInstituicao()!=null && object.getInstituicao().getId()!=null) ||
                StringUtils.isNotBlank(object.getNome())
            );
    }
}
