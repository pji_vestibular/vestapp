package com.vestapp.bc;

import com.vestapp.dao.ResumoDao;
import com.vestapp.model.Resumo;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class ResumoBc extends PatternBc<Resumo>{
    
    private static final ResumoBc instance = new ResumoBc();
    
    private ResumoBc(){
    }
    
    public static ResumoBc getInstance(){
        return instance;
    }

    @Override
    public void insert(Resumo objeto) {
        validateObject(objeto);
        ResumoDao.getInstance().insert(objeto);
    }

    @Override
    public void update(Resumo objeto) {
        validateObject(objeto);
        ResumoDao.getInstance().update(objeto);
    }

    @Override
    public void delete(Resumo objeto) {
        if(findById(objeto.getId())!=null){
            ResumoDao.getInstance().delete(objeto);
        }
    }

    @Override
    public List<Resumo> findAll() {
        return ResumoDao.getInstance().findAll();
    }

    @Override
    public Resumo findById(int id) {
        return ResumoDao.getInstance().findById(id);
    }

    @Override
    public List<Resumo> findByFilter(Resumo filter) {
        if(!validateFilter(filter)){
            throw new RuntimeException("É necessario preencher ao menos um campo de busca");
        }
        return ResumoDao.getInstance().findByFilter(filter);
    }

    @Override
    protected void validateObject(Resumo object) {
        if(object == null){
            throw new RuntimeException("Resumo nulo");
        }
        if(StringUtils.isBlank(object.getNome())){
            throw new RuntimeException("Insira o titulo do resumo");
        }
        if(StringUtils.isBlank(object.getTexto())){
            throw new RuntimeException("O contúdo do resumo está em branco");
        }
        if(object.getData()!=null){
            throw new RuntimeException("Data nula");
        }
        if(object.getProfessor()!=null){
            throw new RuntimeException("Professor nulo");
        }
        if(object.getMateria()!=null){
            throw new RuntimeException("É necessario que uma materia seja selecionada");
        }
    }

    @Override
    protected boolean validateFilter(Resumo object) {
        return object != null && (StringUtils.isNotBlank(object.getNome()) ||
                StringUtils.isNotBlank(object.getTexto()) ||
                object.getData()!=null ||
                (object.getMateria()!=null && object.getMateria().getId()!=0) ||
                (object.getProfessor()!=null && object.getProfessor().getId()!=0)
            );
    }
    
}
