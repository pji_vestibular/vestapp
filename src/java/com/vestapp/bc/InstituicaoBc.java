package com.vestapp.bc;

import com.vestapp.dao.InstituicaoDao;
import com.vestapp.model.Instituicao;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class InstituicaoBc extends PatternBc<Instituicao>{
    
    private static final InstituicaoBc instance = new InstituicaoBc();
    
    private InstituicaoBc(){
    }
    
    public static InstituicaoBc getInstance(){
        return instance;
    }

    @Override
    public void insert(Instituicao objeto) {
        validateObject(objeto);
        InstituicaoDao.getInstance().insert(objeto);
    }

    @Override
    public void update(Instituicao objeto) {
        validateObject(objeto);
        InstituicaoDao.getInstance().update(objeto);
    }

    @Override
    public void delete(Instituicao objeto) {
        if(findById(objeto.getId())!=null){
            InstituicaoDao.getInstance().delete(objeto);
        }
    }

    @Override
    public List<Instituicao> findAll() {
        return InstituicaoDao.getInstance().findAll();
    }

    @Override
    public Instituicao findById(int id) {
        return InstituicaoDao.getInstance().findById(id);
    }

    @Override
    public List<Instituicao> findByFilter(Instituicao filter) {
        if(!validateFilter(filter)){
            throw new RuntimeException("É necessario preencher ao menos um campo de busca");
        }
        return InstituicaoDao.getInstance().findByFilter(filter);
    }

    @Override
    protected void validateObject(Instituicao object) {
        if(object == null){
            throw new RuntimeException("Instituição nula");
        }
        if(StringUtils.isBlank(object.getCodigo())){
            throw new RuntimeException("Codigo nulo");
        }
        if(StringUtils.isBlank(object.getNome())){
            throw new RuntimeException("Nome nulo");
        }
    }

    @Override
    protected boolean validateFilter(Instituicao object) {
        return object != null && (StringUtils.isNotBlank(object.getCodigo()) ||
                StringUtils.isNotBlank(object.getNome())
            );
    }
    
}
