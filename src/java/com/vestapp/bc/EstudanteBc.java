package com.vestapp.bc;

import com.vestapp.bc.utils.Validacoes;
import com.vestapp.dao.EstudanteDao;
import com.vestapp.model.Estudante;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class EstudanteBc extends PatternBc<Estudante>{
    
    private static final EstudanteBc instance = new EstudanteBc();
    
    private EstudanteBc() {
    }
    
    public static EstudanteBc getInstance(){
        return instance;
    }

    public Estudante login(Estudante estudante){
        if(!validateFilter(estudante)){
            throw new RuntimeException("É necessario preencher ao menos um campo");
        }
        List<Estudante> estudantes = EstudanteDao.getInstance().findByFilter(estudante);
        if(estudantes.size() < 1){
            throw new RuntimeException("Estudante não encontrado");
        }else{
            if(!estudantes.get(0).getSenha().equals(estudante.getSenha())){
                throw new RuntimeException("Senha inválida");
            }else{
                return estudantes.get(0);
            }
        }
    }
    
    @Override
    public void insert(Estudante objeto) {
        validateObject(objeto);
        EstudanteDao.getInstance().insert(objeto);
    }

    @Override
    public void update(Estudante objeto) {
        validateObject(objeto);
        EstudanteDao.getInstance().update(objeto);
    }

    @Override
    public void delete(Estudante objeto) {
        if(findById(objeto.getId())!=null){
            EstudanteDao.getInstance().delete(objeto);
        }
    }

    @Override
    public List<Estudante> findAll() {
        return EstudanteDao.getInstance().findAll();
    }

    @Override
    public Estudante findById(int id) {
        return EstudanteDao.getInstance().findById(id);
    }
    
    @Override
    public List<Estudante> findByFilter(Estudante filter) {
        if(!validateFilter(filter)){
            throw new RuntimeException("É necessario preencher ao menos um campo de busca");
        }
        return EstudanteDao.getInstance().findByFilter(filter);
    }

    @Override
    protected void validateObject(Estudante object) {
        if(object == null){
            throw new RuntimeException("Estudante nulo");
        }
        if(StringUtils.isBlank(object.getLogin())){
            throw new RuntimeException("Login nulo");
        }
        if(StringUtils.isBlank(object.getEmail())){
            throw new RuntimeException("Email nulo");
        }
        if(!Validacoes.ValidarEmail(object.getEmail())){
            throw new RuntimeException("Email inválido");
        }
        if(StringUtils.isBlank(object.getNome())){
            throw new RuntimeException("Nome nulo");
        }
        if(object.getDataNascimento() == null){
            throw new RuntimeException("Data de nascimento nula");
        }
        if(object.getDataNascimento().after(new Date())){
            throw new RuntimeException("Data de nascimento inválida");
        }
        if(StringUtils.isBlank(object.getSenha())){
            throw new RuntimeException("Senha nula");
        }
        if(object.getSenha().trim().length() < 6){
            throw new RuntimeException("Senha muito curta");
        }
    }

    @Override
    protected boolean validateFilter(Estudante object) {
        return object != null && (StringUtils.isNotBlank(object.getNome()) ||
                StringUtils.isNotBlank(object.getNomeColegio()) ||
                StringUtils.isNotBlank(object.getNumeroMatricula()) ||
                StringUtils.isNotBlank(object.getLogin()) ||
                StringUtils.isNotBlank(object.getEmail()) ||
                object.getDataNascimento() != null
            );
    }
    
    protected void validateExistingUser(Estudante object){
        Estudante filtro = new Estudante();
        filtro.setLogin(object.getLogin());
        if(findByFilter(filtro).size()>0){
            throw new RuntimeException("Login já utilizado");
        }
        filtro.setLogin(null);
        filtro.setEmail(object.getEmail());
        if(findByFilter(filtro).size()> 0){
            throw new RuntimeException("Email já cadastrado");
        }
    }
    
}
