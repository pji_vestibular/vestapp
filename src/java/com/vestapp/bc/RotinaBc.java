package com.vestapp.bc;

import com.vestapp.dao.RotinaDao;
import com.vestapp.model.Rotina;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class RotinaBc extends PatternBc<Rotina>{
    
    private static final RotinaBc instance = new RotinaBc();
    
    private RotinaBc(){
    }
    
    public static RotinaBc getInstance(){
        return instance;
    }

    @Override
    public void insert(Rotina objeto) {
        validateObject(objeto);
        RotinaDao.getInstance().insert(objeto);
    }

    @Override
    public void update(Rotina objeto) {
        validateObject(objeto);
        RotinaDao.getInstance().update(objeto);
    }

    @Override
    public void delete(Rotina objeto) {
        if(findById(objeto.getId())!=null){
            RotinaDao.getInstance().delete(objeto);
        }
    }

    @Override
    public List<Rotina> findAll() {
        return RotinaDao.getInstance().findAll();
    }

    @Override
    public Rotina findById(int id) {
        return RotinaDao.getInstance().findById(id);
    }

    @Override
    public List<Rotina> findByFilter(Rotina filter) {
        if(!validateFilter(filter)){
            throw new RuntimeException("É necessario preencher ao menos um campo de busca");
        }
        return RotinaDao.getInstance().findByFilter(filter);
    }

    @Override
    protected void validateObject(Rotina object) {
        if(object == null){
            throw new RuntimeException("Rotina nulo");
        }
        if(object.getConcurso()!=null){
            throw new RuntimeException("É necessario selecionar um concurso");
        }
        if(object.getEstudante()!=null){
            throw new RuntimeException("Estudante nulo");
        }
        if(object.getDataInicio()!=null){
            throw new RuntimeException("Selecione uma data inicial");
        }
        if(object.getDataInicio().before(new Date())){
            throw new RuntimeException("A data inicial não pode ser anterior a data atual");
        }
        if(object.getDataInicio().after(object.getConcurso().getData())){
            throw new RuntimeException("A data inicial não pode ser depois da data do concurso");
        }
        if(object.getDataFinal()!=null){
            throw new RuntimeException("Selecione uma data final");
        }
        if(object.getDataFinal().before(new Date())){
            throw new RuntimeException("A data final não pode ser anterior a data atual");
        }
        if(object.getDataFinal().after(object.getConcurso().getData())){
            throw new RuntimeException("A data final não pode ser depois da data do concurso");
        }
    }

    @Override
    protected boolean validateFilter(Rotina object) {
        return object != null && (StringUtils.isNotBlank(object.getDescricao()) ||
                object.getDataInicio()!=null ||
                object.getDataFinal()!=null ||
                (object.getEstudante()!=null && object.getEstudante().getId()!=0) ||
                (object.getConcurso()!=null && object.getConcurso().getId()!=0)
            );
    }
}
