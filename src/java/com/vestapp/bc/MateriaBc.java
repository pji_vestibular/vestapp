package com.vestapp.bc;

import com.vestapp.dao.MateriaDao;
import com.vestapp.model.Materia;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class MateriaBc extends PatternBc<Materia>{
    
    private static final MateriaBc instance = new MateriaBc();
    
    private MateriaBc(){
    }
    
    public static MateriaBc getInstance(){
        return instance;
    }

    @Override
    public void insert(Materia objeto) {
        validateObject(objeto);
        MateriaDao.getInstance().insert(objeto);
    }

    @Override
    public void update(Materia objeto) {
        validateObject(objeto);
        MateriaDao.getInstance().update(objeto);
    }

    @Override
    public void delete(Materia objeto) {
        if(findById(objeto.getId())!=null){
            MateriaDao.getInstance().delete(objeto);
        }
    }

    @Override
    public List<Materia> findAll() {
        return MateriaDao.getInstance().findAll();
    }

    @Override
    public Materia findById(int id) {
        return MateriaDao.getInstance().findById(id);
    }

    @Override
    public List<Materia> findByFilter(Materia filter) {
        if(!validateFilter(filter)){
            throw new RuntimeException("É necessario preencher ao menos um campo de busca");
        }
        return MateriaDao.getInstance().findByFilter(filter);
    }

    @Override
    protected void validateObject(Materia object) {
        if(object == null){
            throw new RuntimeException("Materia nula");
        }
        if(StringUtils.isBlank(object.getNome())){
            throw new RuntimeException("Nome nulo");
        }
        if(StringUtils.isBlank(object.getConteudo())){
            throw new RuntimeException("Conteúdo nulo");
        }
        if(object.getConcurso()!=null){
            throw new RuntimeException("É necessario que a materia possua um concurso");
        }
    }

    @Override
    protected boolean validateFilter(Materia object) {
        return object != null && (StringUtils.isNotBlank(object.getNome()) ||
                StringUtils.isNotBlank(object.getConteudo()) ||
                (object.getConcurso()!=null && object.getConcurso().getId()!=null)
            );
    }
    
}
