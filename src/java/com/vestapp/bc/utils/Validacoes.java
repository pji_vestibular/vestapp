package com.vestapp.bc.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validacoes {
    
    static{   
    }
    
    public static boolean ValidarEmail(String email){
        Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$"); 
        Matcher m = p.matcher(email); 
        return m.find();
    }
}
