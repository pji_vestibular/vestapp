<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#/">Home</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#">Home</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Rotinas<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Todas</a></li>
            <li><a href="#">Hoje</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Nova Rotina</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Professores</a></li>
            <li><a href="#">Instituicoes</a></li>
            <li><a href="#">Concursos</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Materias</a></li>
            <li><a href="#">Resumos</a></li>
          </ul>
        </li>
      </ul>
      
      <form action="" method="post" class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="filtro" placeholder="Search" name="valor">
        </div>
        <button type="submit" class="btn btn-default">Pesquisar</button>
      </form>
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Minha Conta</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Op��es <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Alterar</a></li>
            <li role="separator" class="divider"></li>
            <li>
                <a href="estudante/sair" method="post">
                    Sair <span class="glyphicon glyphicon-log-out"></span>
                </a>
            </li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>