<%-- 
    Document   : EstudanteView
    Created on : 13/11/2016, 21:28:17
    Author     : Alexandre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset=ISO-8859-1">

        <script src="js/jquery-1.12.3.min.js" type="text/javascript"></script>
        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/pages/menus/EstudanteMenu.jsp" %>
        
    </body>
</html>
