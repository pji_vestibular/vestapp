<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="ISO-8859-1">

        <script src="js/jquery-1.12.3.min.js" type="text/javascript"></script>
        <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>

        <title>Login</title>
    </head>
    <body>
    <%@ include file="/WEB-INF/pages/menus/HomeMenu.jsp" %>
    ${msg}
    <div class="container">
            <div class="col-xs-8 col-xs-offset-2">
                    <form action="login" method="post">
                            <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <div class="form-group">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-primary">
                                                <input type="radio" name="loginType" id="professor" autocomplete="off"> Professor <span class="glyphicon glyphicon-apple"/>
                                            </label>
                                            <label class="btn btn-primary active">
                                              <input type="radio" name="loginType" id="estudante" autocomplete="off" checked> Estudante <span class="glyphicon glyphicon-book"/>
                                            </label>
                                         </div>
                                        </div>
                                        <div class="form-group">
                                                <label for="login" id="usuariolb">Login:</label>
                                                <input type="text" name="login"  id="login" class="form-control" placeholder="informe seu login">
                                        </div>
                                        <div class="form-group">
                                                <label for="senha" id="senhalb">Senha:</label>
                                                <input type="password" name="senha" id="senha" class="form-control" placeholder="informe uma senha">
                                        </div>
                                    </div>
                                    <div class="panel-footer text-center">
                                            <input type="submit" value="login" class="btn btn-info">
                                    </div>
                            </div>
                    </form>
            </div>
    </div>	
    </body>
</html>